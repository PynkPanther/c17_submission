% Template for ISBI-2017 paper; to be used with:
%          spconf.sty  - ICASSP/ICIP LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------
\documentclass{article}
\usepackage{spconf,amsmath,graphicx}
\usepackage[utf8]{inputenc} % u.a. Umlaute
\usepackage[ngerman]{babel} % Deutsche Worttrennugn, Anführungszeichen
\usepackage{xcolor} % Für Farben
\usepackage{lipsum} % Fülltext mit \lipsum[1]



% Example definitions.
% --------------------
\def\x{{\mathbf x}}
\def\L{{\cal L}}

% Title.
% ------
\title{Convolutional Neural Networks and Random Forests for detection and classification of metastasis in histological slides}
%
% Single address.
% ---------------
\name{\textit{Jonas Annuscheit\textsuperscript{2,*}, Klaus Strohmenger\textsuperscript{1,*},}\\\textit{Iris Klempert\textsuperscript{3}, Benjamin Voigt\textsuperscript{2},}\\\textit{Christian Herta\textsuperscript{1,+}, Peter Hufnagl\textsuperscript{1,2,3,+}}}
\address{\textsuperscript{1}Hochschule für Technik und Wirtschaft Berlin, Univeristy of Applied Sciences\\\textsuperscript{2}Centrum für Biomedizinische Bild- und Informationsverarbeitung (CBMI) \\\textsuperscript{3}Charité – Universitätsmedizin Berlin \\ Jonas.Annuscheit@gmail.com, Klaus.Strohmenger@gmail.com}
%
% For example:
% ------------
%\address{School\\
%	Department\\
%	Address}
%
% Two addresses (uncomment and modify for two-address case).
% ----------------------------------------------------------
%\twoauthors
%  {A. Author-one, B. Author-two\sthanks{Thanks to XYZ agency for funding.}}
%	{School A-B\\
%	Department A-B\\
%	Address A-B}
%  {C. Author-three, D. Author-four\sthanks{The fourth author performed the work
%	while at ...}}
%	{School C-D\\
%	Department C-D\\
%	Address C-D}
%
% More than two addresses
% -----------------------
% \name{Author Name$^{\star \dagger}$ \qquad Author Name$^{\star}$ \qquad Author Name$^{\dagger}$}
%
% \address{$^{\star}$ Affiliation Number One \\
%     $^{\dagger}$}Affiliation Number Two
%
\begin{document}
%\ninept
%
\maketitle
%
\begin{abstract}

For many malignant (metastatic) tumors, e.g. for breast cancer, the most common route of metastasis is the lymphatic system. For subsequent treatment decisions, it is essential to examine the lymph nodes surrounding the primary tumor with a microscope for metastatic infestation. The number of affected lymph nodes and the severity of the infestation are summarized by a pN stage designation.
This process is performed by pathologists who need years of training to identify the key features. Nevertheless, it is a time-consuming and error-prone process. Today, slides with tissue can be digitized at a quality which corresponds approximately to the current state of light microscopy. In the present work we present a proof of concept, which completely automates the process from digitization to the classification of a patient into the pN- category. Various image processing methods and methods of machine learning and deep learning were tested and evaluated on the CAMEYLON 16 and 17 dataset.

\end{abstract}
%
\begin{keywords}
Camelyon Challenge, Convolutional Neural Network, Random Forest, histological slide, metastasis detection
\end{keywords}
%
\section{Introduction}
\label{sec:intro}

Breast cancer is the most common cancer for women. Approximately 12\% of all women fall ill in their lives to breast cancer or one of its precursors. Therapy response and overall prognosis are highly dependent on tumor size, lymph node infestation and metastasis status (see \cite[p.379ff]{Genzwuerker2014}). The \emph{pN} stage describes the status of lymph node infestation and is part of the TNM system (Tumor Nodes Metastasis) for the staging of malignant tumors, which in turn is the main basis for the subsequent therapeutic decisions. \emph{T} describes the primary tumor, \emph{N} (or here \emph{pN}) the nodal lymph node infestation and \emph{M} the metastasis status (see \cite[p.108f]{Herold2007}).

\subsection{pN stages}

To determine the pN stage, several lymph nodes surrounding the primary tumor have to be examined. If a metastasis region is detected in one of the slides of theses lymph nodes, the cells contained in that region are counted and / or the size of the region is measured. If the region contains less than 200 cells or is smaller than $0.2 mm$, it is classified as \emph{itc} (isolated tumor cell). If it contains more than 200 cells and its size it in between $0.2 mm$ and $2.0 mm$, it is classified as \emph{micro metastasis}. If it is greater, its class is \emph{macro metastasis}. The number of slides classified into each of these classes, then determines the patient's pN stage. Table (\ref{tab:pnstages}) shows the rules for pN stage assignment.
\begin{table}[!htbp]
\small
\caption{The possible pN stages.}
	\begin{tabular}{p{\dimexpr0.09\textwidth-2\tabcolsep-\arrayrulewidth\relax}
                p{\dimexpr0.395\textwidth-2\tabcolsep-\arrayrulewidth\relax}}
		\hline
		pN stage & description \\
		\hline
		\hline
		pN0 & No itcs, micro or macro metastasis found \\
		pN0(i+) & Only itcs found. \\
		pN1mi & Micro metastasis found, but no macro metastasis \\
		pN1 & Metastasis found in 1-3 lymph nodes. At least 1 is a macro metastasis \\
		pN2 & Metastasis found in more than 3 lymph nodes. At least 1 is a macro metastasis \\
		\hline
	\end{tabular}
	\label{tab:pnstages}
\end{table}

\section{Methodology}

On the following pages we propose a framework to automatically predict the patient's pN stage. Several machine learning and deep learning techniques are combined, like convolutional neural networks (CNN) and random forests \cite{Breiman2001}. Figure (\ref{abb:overview}) shows and overview of our framework.
\begin{figure*}
  \includegraphics[width=\textwidth,height=4cm]{overview}
\caption{Overview of the proposed framework. Patient pN-stage classification is left out here, because it is a simple rule based assignment once the slide labels \emph{normal}, \emph{itc}, \emph{micro} and \emph{macro} are obtained.}
\label{abb:overview}
\end{figure*}

\subsection{Dataset}

The CAMELYON challenge provides two datasets: The CAMELYON16 dataset \cite{C162017} and the CAMELYON17 dataset \cite{C172017}. The CAMELYON16 training set consists of 270 slides, which are annotated with the labels \emph{normal} and \emph{tumor}. For slides of the label \emph{tumor} there also exist masks, which localize the metastasis regions. The CAMELYON16 test set consists of 130 slides, which are also annotated with the labels \emph{normal}, \emph{micro} and \emph{macro}. Examples with the label \emph{itc} are not to be found in there. The CAMELYON17 training set consists of 100 patients with five slides per patient. This set includes examples for the class itc. The test set also contains 100 patients with five slides per patient. It's labels are unknown. Predicting them is the goal of the CAMELYON challenge.

\subsection{Preprocessing}

Our approach was to mainly train our CNN model online.
For this purpose, we only performed the tissue segmentation and the stain normalization
before the training started. All other necessary preprocessing steps like HSD color transformation, hard example mining and data augmentation were done online during the training phase.

For \emph{tissue segmentation} we used the otsu threshold algorithm. To use the otsu
algorithm, we transfer the RGB color space into a gray image with the following
equation for each pixel: $gray = red + green - (2 * blue)$
Then we calculated a threshold value $\sigma$ for the generated gray image with
the otsu algorithm. To make sure we do not miss a metastasis region at the border,
we divide the threshold value: $\sigma_{low} = \sigma/4$.
All regions that can be created with the threshold value $\sigma_{low}$ and have
one or more pixels with a threshold value greater than $\sigma$ were considered tissue.
This reduces the area of the slide to $16.3\%$ in average of its original size without losing a metastasis areas.
For quick access to a random position during training, we have created a bounding box tree for each slide.

The \emph{stain normalization} helped to reduce the different stains that came from
the different scanners. Babak Ehteshami Bejnordi's algorithm has done a great job here \cite{bejnordi2016stain}.
Babak's algorithm includes finding and counting cells, classifying the image and creating look-up tables (LUTs).
These LUTs can be used online to normalize the slide or parts of the slide.

During training, we applied \emph{hard example mining} by using patches with false-positive and small metastasis areas more often.
We used the LUTs and transferred the RGB into an \emph{HSD} color space \cite{van2000hue}.
Then we manipulate the image by \emph{rotating}, \emph{mirroring} and \emph{pixel manipulation}.

\subsection{CNN-Architecture}

\begin{figure*}
\begin{center}
  \includegraphics[width=15cm]{cnn_2}
\caption{Construction of the final trained CNN model. Three Atrous layers (green), a 
slice layer (red), a roll layer (dark red), a stack layer (brown), and a fully connected 
layer (orange) were used in one Inception-V4 model. The two used Inception-V4 models have received different resolution 
levels as input and are merged with a fully convolutional layer (yellow), so that a decision can be made for these 
whether the current region is metastasis or not.}
\end{center}
\label{abb:cnn}
\end{figure*}

For the online learning we used two Inception-V4 models as CNNs. Both get input images that are $312x312x3$ in size.
One network receives the highest resolution and the other the second highest resolution.
Both networks are connected at the end with a fully connected layer.
This technique was introduced and applied by Yun Liu et al. \cite{liu2017detecting}.

We have modified the Inception-V4 models and extended them by several layers.
First we changed the first three convolutional layers and turned them into atrous
layers \cite{DBLP:journals/corr/ChenPKMY14} to get more overlapping between the pixels.
Then we add the roll, stack and slice layers to the network \cite{dieleman2016exploiting}.
This technique makes the network more rotation invariant, but the network becomes much larger.
The size of the model was increased by using batch normalization and the adam learning algorithm \cite{Kingma2014}.
Due to the increased network size we had to reduce the batch size to 8 images during training.

\subsection{Heatmap}
The trained model was used to create heatmaps for the slides. For these heatmaps one pixel presents a 256x256 patch of the maximum resolution. On average, our model produces six heatmaps per hour.

\subsection{Slide classification}
\label{ssec:slidecla}

%\subsubsection{KLAUS Heatmap preprocessing}
Overall we extracted 10 Features from the heatmaps. Some features can be extracted on the raw heatmap. These are \emph{(1) highest probability}, \emph{(2) sum of all probabilities} and \emph{(3) avg probability}. For the remaining seven features the heatmap has to be segmented before. The segmentation process consists of applying a global threshold \emph{t}, applying morphological opening \emph{o} times and morphological closing \emph{c} times. For the preprocessing process we invented a method to determine the quality of the preprocessing process before applying a classifier:
\begin{enumerate}
	\item Subtract the pathologists masks from the binarized heatmap, whichs yields an error mask.
	\item Sum the error pixels and divide by the number of pixels of the pathologists mask.
	\item Sum over all processed heatmaps.
\end{enumerate}

%\subsubsection{KLAUS Feature extraction}

On the segmented heatmaps we extracted the features \emph{(4) total area}, \emph{(5) major axis of the biggest connected region}, \emph{(6) minor axis of the biggest connected region}, \emph{(7) area of the biggest connected region}, \emph{(8) major axis of the second biggest connected region}, \emph{(9) minor axis of the second biggest connected region} and \emph{(10) area of the second biggest connected region}.

With the extracted features another classifier was trained to predict the slide labels \emph{normal}, \emph{itc}, \emph{micro} and \emph{macro}. In our final model we used a random forest for this task, though other techniques were also evaluated (see next section). The last task, the pN-stage classification for each patient is a simple rule based assignment once the slide labels are obtained.

\section{Results}

We used the CAMELYON16 test dataset to evaluate the CNN model, which were used to generate the heatmaps.
For the FROC curve, every cnn prediction for a metastasis region was used. The FROC's AUC is $0.6392$.
If many errors are allowed in the FROC curve, it turned out that almost all metastatic regions were found, but the number of false positives was enormous.
The ROC curve was generated by using the highest value of each slide as a prediction that this slide is a tumor slide or not. The ROC's AUC is $0.9829$.

\begin{figure}[htb]
\begin{center}
\begin{minipage}[b]{1\linewidth}
  \centering
  \centerline{\includegraphics[width=7.5cm]{ROC_small}}
%  \vspace{2.0cm}
%  \centerline{}\medskip
\end{minipage}
\end{center}
\caption{The ROC curve shows the results for the CAMELYON16 test dataset using the evaluation script from the CAMELYON16 website. The ROC's AUC is $0.9829$.}
\label{fig:roc}
\end{figure}


% \begin{figure}[htb]
% \begin{minipage}[b]{1.0\linewidth}
%   \centering
%   \centerline{\includegraphics[width=6.5cm]{FROC_8}}
% %  \vspace{2.0cm}
% %  \centerline{}\medskip
% \end{minipage}
% \caption{The FROC curve was created with the code from the CAMELYON16 on the CAMELYON16 test data set. The AUC value is 
% 0.6392}
% \label{fig:froc}
% \end{figure}

For the slide classifier we evaluated \emph{random forests} and \emph{multi layer perceptrons}. Both were trained with the 10 features mentioned. Additionally, we also tested several \emph{convolutional neural networks} and \emph{fully convolutional networks} for the slide classifier with the raw heatmaps as input. The dataset consisted of the CAMELYON17 training set and the CAMELYON16 test set. We used 10-fold-crossvalidation.

In total we evaluated over 240 different parameter settings in the preprocessing of the heatmaps each with over 25 parameter settings for the slide classifier. Random forests seem to work best with \emph{t=0.4}, \emph{o=0} and \emph{c=0}. Multi layer perceptrons seem to prefer a higher threshold \emph{t=0.6} to \emph{0.7}, combined with morphological operations \emph{o=2} and \emph{c=2}. The best performing convolutional neural network for slide classification was a heavily simplified AlexNet  \cite{Krizhevsky2012} without fully connected layers, which makes it a fully convolutional network. The simplification consisted of reducing the number of kernels in each layer to prevent overfitting. The overall best classifier was a random forest with \emph{1.500} trees and a maximum tree depth of \emph{4}. Table (\ref{tab:resultsslideclassifier}) shows the best results for each classification algorithm.
\begin{table}[!htbp]
\caption{Best results for each classification algorithm for pN stage classification (kappa score) and slide classification (accuracy) on the training set (10 fold cross validation).}
	\begin{tabular}{p{\dimexpr0.21\textwidth-2\tabcolsep-\arrayrulewidth\relax}
	p{\dimexpr0.16\textwidth-2\tabcolsep-\arrayrulewidth\relax}
	p{\dimexpr0.11\textwidth-2\tabcolsep-\arrayrulewidth\relax}
}
		\hline
		Classifier & Kappa score & Accuracy  \\
		\hline		
		\hline
		Random forest & 0.850 & 0.871 \\
		Multi layer perceptron & 0.822 & 0.870 \\
		Fully conv. network & 0.752 & 0.801 \\
		\hline
	\end{tabular}
	\label{tab:resultsslideclassifier}
\end{table}

The proposed formula in (\ref{ssec:slidecla}) for calculating a preprocessing error of  the heatmaps turned out to be useful. Figure (\ref{fig:preprerror}) shows the context between kappa score and the preprocessing error.
\begin{figure}[htb]
\begin{minipage}[b]{1.0\linewidth}
  \centering
  \centerline{\includegraphics[width=8.0cm]{prepr_kappa_all_cla}}
%  \vspace{2.0cm}
%  \centerline{}\medskip
\end{minipage}
\caption{Context between kappa score (x axis) and preprocessing error (y axis). Results of more than 6.000 different parameter settings are shown.}
\label{fig:preprerror}
\end{figure}

\section{Discussion}

We were impressed by our model that it does not require any postprocessing of the heatmap to get a really good ROC's AUC value.
On the other hand, we get a low FROC's AUC value. We think that this value can easily be increased by postprocessing. Therefore, a summary of the regions would help.

For random forests in the slide classification, morphological operations turned out not to be useful. We estimate, that noise in the heatmaps has a relatively low impact on results, since the only feature influenced by it is \emph{(4) total area} (after thresholding).

We estimated beforehand that a CNN (or FCN) would not be able to compete with random forests for slide classification. A kappa score of \emph{0.752} is beyond expectations and shows that this approach could become a viable option with bigger training sets. 


\section{Conclusion}

In this article we presented a way to generate heatmaps and make a prediction for each slide (\emph{normal}, \emph{micro} and \emph{macro}) and for each patient (\emph{pN-stage)}.
We developed an effective way to predict regions for the FROC curve and improve them by reducing false positives.

Improvement possibilities are seen above all in the preprocessing of the heatmaps. It has been found that by careful selection of the binarization threshold, morphological operations to connect nearby regions are superfluous. Instead of morphological operations, smarter methods should be tested. Possible solution approaches to segmentation could be the use of conditional random fields}, fully convolutional networks or clustering algorithms.


\section{Acknowledgements}
We would like to thank Babak Ehteshami Bejnordi for his great help and work in the stain normalization process.
%\include{ieeeVorlage}

% References should be produced using the bibtex program from suitable
% BiBTeX files (here: strings, refs, manuals). The IEEEbib.bst bibliography
% style file from IEEE produces unsorted bibliography list.
% -------------------------------------------------------------------------
\bibliographystyle{IEEEbib}
\bibliography{strings,refs}

\end{document}
